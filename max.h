#ifndef MAX_H
#define MAX_H


class Max
{
public:
    Max();
    int comp(int x, int y);
    float comp(float x, float y);
};

#endif // MAX_H
