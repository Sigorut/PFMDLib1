#ifndef MIN_H
#define MIN_H


class Min
{
public:
    Min();
    int comp(int x, int y);
    float comp(float x, float y);
};

#endif // MIN_H
